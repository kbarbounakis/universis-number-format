# universis-number-format

Universis library for converting numbers to text

## Usage

    npm i @universis/number-format

Use NumberFormatter class to convert a number to cardinal: 

    import {NumberFormatter} from '@universis/number-format';
    const text = new NumberFormatter().format(54.5, 'en');
    // => fifty-four and five tenths

or define fractional digits

    import {NumberFormatter} from '@universis/number-format';
    const text = new NumberFormatter().format(9.75, 'en', 3);
    // => nine and seven hundred fifty thousandths

A new locale may be register by using NumberFormatter.registerLocale(locale,data);

    import {NumberFormatter} from '@universis/number-format';
    NumberFormatter.registerLocale('fr', {
        ...
        ["21", "vignt et un"],
        ...
        ["3", "trois"],
        ["2", "deux"],
        ["1", "un"],
        ["0", "zero"],
        ...
    })