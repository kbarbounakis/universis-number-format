export const en = {
    decimalSeparator: "and",
    values: [
        ["1000000000000", {
            "one": "one trillion",
            "many": "trillions"
        }], ["1000000000", {
            "one": "one billion",
            "many": "billions"
        }],
        ["1000000", {
            "one": "one million",
            "many": "millions"
        }],
        ["1000", {
            "one": "one thousand",
            "many": "thousand"
        }],
        ["100", {
            "one": "one hundred",
            "many": "hundred"
        }],
        ["90", "ninety"],
        ["80", "eighty"],
        ["70", "seventy"],
        ["60", "sixty"],
        ["50", "fifty"],
        ["40", "forty"],
        ["30", "thirty"],
        ["20", "twenty"],
        ["19", "nineteen"],
        ["18", "eighteen"],
        ["17", "seventeen"],
        ["16", "sixteen"],
        ["15", "fifteen"],
        ["14", "fourteen"],
        ["13", "thirteen"],
        ["12", "twelve"],
        ["11", "eleven"],
        ["10", "ten"],
        ["9", "nine"],
        ["8", "eight"],
        ["7", "seven"],
        ["6", "six"],
        ["5", "five"],
        ["4", "four"],
        ["3", "three"],
        ["2", "two"],
        ["1", "one"],
        ["0", "zero"],
        ["0.0000001", {
            "one": "one millionth",
            "many": "millionths",
        }],
        ["0.000001", {
            "one": "one hundred-thousandth",
            "many": "hundred-thousandths",
        }],
        ["0.0001", {
            "one": "ten-thousandths",
            "many": "ten-thousandths",
        }],
        ["0.001", {
            "one": "one thousandth",
            "many": "thousandths",
        }],
        ["0.01", {
            "one": "one hundredth",
            "many": "hundredths",
        }],
        ["0.1", {
            "one": "one tenth",
            "many": "tenths",
        }]
    ],
    dictionary: [
        ["twenty\\s(\\w+)", "twenty-$1"],
        ["thirty\\s(\\w+)", "thirty-$1"],
        ["forty\\s(\\w+)", "forty-$1"],
        ["fifty\\s(\\w+)", "fifty-$1"],
        ["sixty\\s(\\w+)", "sixty-$1"],
        ["seventy\\s(\\w+)", "seventy-$1"],
        ["eighty\\s(\\w+)", "eighty-$1"],
        ["ninety\\s(\\w+)", "ninety-$1"],
        ["ty\\-(tenths)", "ty $1"],
        ["ty\\-(hundredths)", "ty $1"],
        ["ty\\-(thousandths)", "ty $1"],
        ["ty\\-(ten\\-thousandths)", "ty $1"],
        ["ty\\-(hundred\\-thousandths)", "ty $1"],
        ["ty\\-(millionths)", "ty $1"],
    ],
    spellcheck(text: string) {
        for (let i = 0; i < this.dictionary.length; i++) {
            const item = this.dictionary[i];
            text = text.replace(new RegExp(item[0], 'g'), item[1]);
        }
        return text;
    }
};