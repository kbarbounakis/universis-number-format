export interface NumberLocale {
    decimalSeparator?: string;
    values: Array<Array<any>>;
    dictionary?: Array<Array<string>>;
    spellcheck?(text: string): string;
}
