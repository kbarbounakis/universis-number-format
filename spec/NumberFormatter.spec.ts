import {NumberFormatter, LOCALES} from "../src";
describe('NumberFormatter', () => {

    it('should create instance', () => {
       const formatter = new NumberFormatter();
       expect(formatter).toBeTruthy();
    });

    it('should throw exception for missing locale', () => {
        const formatter = new NumberFormatter();
        expect(function() {
            formatter.format(100, 'fr');
        }).toThrowError(`The specified locale is missing for registered locales.`);
        NumberFormatter.registerLocale('en', LOCALES.en);
    });

    it('should use format for integers', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(0, 'el')).toBe('μηδέν');
        expect(formatter.format(1010342, 'el')).toBe('ένα εκατομμύριο δέκα χιλιάδες τριακόσια σαράντα δύο');
        expect(formatter.format(102, 'el')).toBe('εκατόν δύο');
        expect(formatter.format(100, 'el')).toBe('εκατό');
        expect(formatter.format(950, 'el')).toBe('εννιακόσια πενήντα');
        expect(formatter.format(1745, 'el')).toBe('χίλια επτακόσια σαράντα πέντε');
        expect(formatter.format(10342, 'el')).toBe('δέκα χιλιάδες τριακόσια σαράντα δύο');
        expect(formatter.format(120342, 'el')).toBe('εκατόν είκοσι χιλιάδες τριακόσια σαράντα δύο');
    });

    it('should use genders', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(13001, 'el')).toBe('δεκατρείς χιλιάδες ένα');
    });

    it('should format number with 1 decimal digits', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(0.4, 'el')).toBe('μηδέν και τέσσερα δέκατα');
        expect(formatter.format(1002.7, 'el')).toBe('χίλια δύο και επτά δέκατα');
    });

    it('should format number with 2 decimal digits', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(0.45, 'el', 3)).toBe('μηδέν και τετρακόσια πενήντα χιλιοστά');
        expect(formatter.format(0.45, 'el')).toBe('μηδέν και σαράντα πέντε εκατοστά');
    });

    it('should format number with 3 decimal digits', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(9.075, 'el')).toBe('εννέα και εβδομήντα πέντε χιλιοστά');
        expect(formatter.format(9.075, 'el', 4)).toBe('εννέα και επτακόσια πενήντα δεκάκις χιλιοστά');
    });

    it('should format number (en)', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(250, 'en')).toBe('two hundred fifty');
        expect(formatter.format(1340, 'en')).toBe('one thousand three hundred forty');
        expect(formatter.format(12520, 'en')).toBe('twelve thousand five hundred twenty');
        expect(formatter.format(345001, 'en')).toBe('three hundred forty-five thousand one');
        expect(formatter.format(1045000, 'en')).toBe('one million forty-five thousand');

        expect(formatter.format(0.3, 'en')).toBe('zero and three tenths');
        expect(formatter.format(1.8, 'en')).toBe('one and eight tenths');
        expect(formatter.format(6.75, 'en')).toBe('six and seventy-five hundredths');
        expect(formatter.format(9.75, 'en', 3)).toBe('nine and seven hundred fifty thousandths');
        expect(formatter.format(13.450, 'en', 3)).toBe('thirteen and four hundred fifty thousandths');
        expect(formatter.format(1016.104, 'en', 3)).toBe('one thousand sixteen and one hundred four thousandths');
    });

    it('should ignore fractional part if zero', () => {
        const formatter = new NumberFormatter();
        expect(formatter.format(7, 'el', 2)).toBe('επτά');
        expect(formatter.format(7.5, 'el', 2)).toBe('επτά και πενήντα εκατοστά');
    });



});